/* _________________________________________________________________________________________________________________

====================================================================================================================
                                                EXPLICACIONES
====================================================================================================================

1. Button
    Al haber introducido los números para actualizar los porcentajes, se llevarán acabo todos los condicionales
    del interior de esta función.

    1.1. New Progress
        Todos estos condicionales sirven para cambiar los valores y progreso de la tabla de progresos que hay
        en la página. Al añadirse un nuevo número del 1 al 100, se actualizará el tamaño de la barra y el
        número que se muestra en su interior.

    1.2 Complete Pogress
        Cuando cualquiera de las barras de progreso llegue al 100, la imágen de la respectiva "raza" saldrá
        abajo de la página, señalando así haber completado su progreso personal.

    Links útiles:
        https://www.tjvantoll.com/2013/03/14/better-ways-of-comparing-a-javascript-string-to-multiple-values/
        https://api.jquery.com/each/

__________________________________________________________________________________________________________________ */

// Button
$(document).on('click', '#update', function(){

    var race = ;
    
    // New Progress
    if($('#' + race).val() != ""){

        var newprogressaria = $('#' + race).val();
        var newprogresswidth = $('#' + race).val() + "%";

        $('#' + race + 'bar').attr('aria-valuenow', newprogressaria).css('width',newprogresswidth);
        $('#' + race + 'bar').html($('#' + race).val()) + "%";

    }

    if($('#highelf').val() != ""){

        var newprogressaria = $('#highelf').val();
        var newprogresswidth = $('#highelf').val() + "%";

        $('#highelfbar').attr('aria-valuenow', newprogressaria).css('width',newprogresswidth);
        $('#highelfbar').html($('#highelf').val()) + "%";

    }

    if($('#dwarf').val() != ""){

        var newprogressaria = $('#dwarf').val();
        var newprogresswidth = $('#dwarf').val() + "%";

        $('#dwarfbar').attr('aria-valuenow', newprogressaria).css('width',newprogresswidth);
        $('#dwarfbar').html($('#dwarf').val()) + "%";

    }

    if($('#nightelf').val() != ""){

        var newprogressaria = $('#nightelf').val();
        var newprogresswidth = $('#nightelf').val() + "%";

        $('#nightelfbar').attr('aria-valuenow', newprogressaria).css('width',newprogresswidth);
        $('#nightelfbar').html($('#nightelf').val()) + "%";

    }

    if($('#halfelf').val() != ""){

        var newprogressaria = $('#halfelf').val();
        var newprogresswidth = $('#halfelf').val() + "%";

        $('#halfelfbar').attr('aria-valuenow', newprogressaria).css('width',newprogresswidth);
        $('#halfelfbar').html($('#halfelf').val()) + "%";

    }

    // Complete progress
    if($('#humanbar').html() == 100){
        $("#humanimg").attr("src","img/human.gif");
    }  
    if($('#highelfbar').html() == 100){
        $("#highelfimg").attr("src","img/highelf.gif");
    }  
    if($('#dwarfbar').html() == 100){
        $("#dwarfimg").attr("src","img/dwarf.gif");
    }  
    if($('#nightelfbar').html() == 100){
        $("#nightelfimg").attr("src","img/nightelf.gif");
    }  
    if($('#halfelfbar').html() == 100){
        $("#halfelfimg").attr("src","img/halfelf.gif");
    }  

})